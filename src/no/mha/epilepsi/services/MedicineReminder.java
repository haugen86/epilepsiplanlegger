package no.mha.epilepsi.services;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import no.mha.epilepsi.handlers.MedicineDBAdapter;
import no.mha.epilepsi.recievers.medicineReciever;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MedicineReminder extends Service {
	private Timer timer = new Timer();
	private MedicineDBAdapter mDbHelper;
	private Cursor curSor;
	private String curDay;
	public Vector med = new Vector();

	public void onCreate() {

		super.onCreate();
		startservice();
		

	}
	
	private void startservice() {
		Log.d("dag", "rofl");
	timer.scheduleAtFixedRate( new TimerTask() {

		public void run() {

		getMedicines();
		Log.d("arr", "cursor is not null!");
		}

		}, 0, 1000*60*60*24);

		; }
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void getMedicines() {
		Calendar calendar = Calendar.getInstance();
		Calendar calendarTimetoStart = (Calendar) calendar.clone();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		Log.d("dag", day+" ");
		
		switch(day) {
			case 1:
				curDay = "Sø";
				break;
			case 2:
				curDay = "Ma";
				break;
			case 3:
				curDay = "Ti";
				break;
			case 4:
				curDay = "On";
				break;
			case 5:
				curDay = "To";
				break;
			case 6:
				curDay = "Fr";
				break;
			case 7:
				curDay = "Lø";
				break;
		}
		
		 mDbHelper = new MedicineDBAdapter(MedicineReminder.this);
         mDbHelper.open();
         try {
         	curSor = mDbHelper.fetchAllMedicines();
         } catch (SQLiteException e) {
         	Log.d("sql", ""+e);
         }
         	if(curSor != null) {
         		//Log.d("arr", "LOLOLOLOL "+curDay);
         		Log.d("sql", "lol");
 	        	curSor.moveToFirst();

 		        while (curSor.isAfterLast() == false) {
 		        	String mDay = curSor.getString(3);
 		        	String[] mDayArr = mDay.split("\\, ");
 		        	
	 		   		int arrLength = mDayArr.length;
	 		   	//	Log.d("arr", ""+arrLength);
	 		   		for(int i = 0;i<arrLength;i++){
	 		   			Log.d("arr", mDayArr[i].toString());
	 		   			String dDay = mDayArr[i].toString();
	 		   			Log.d("arr", "dDay = "+dDay+"!");
	 		   		Log.d("arr", "curDay = "+curDay+"!");
	 		   			if(dDay.equals(curDay)) {
	 		   				Log.d("arr", "DI E LIK!!");
	 		   				Vector v = new Vector();
	 		   				v.add(curSor.getString(0));
	 		   				v.add(curSor.getString(1));
	 		   				v.add(curSor.getString(2));
		 		   			v.add(curSor.getString(3));
		 		   			v.add(curSor.getString(4));
	 		   				med.add(v);
	 		   			
	 		   				Log.d("arr", ((Vector) med.get(0)).get(0).toString());
	 		   				
	 		   			} else {
	 		   			
	 		   			}
	 		   		}
	 		   		curSor.moveToNext();
 		        }
 		        
 		       checkMedicine();
		
         	} else {
         		Log.d("arr", "cursor is null!");
         	}
		
		
	}
	
	public void checkMedicine() {
		boolean b = false;
		Calendar timeOff = Calendar.getInstance();
		Calendar curTime = (Calendar) timeOff.clone();
		int size = med.size();
		for(int i = 0;i<size;i++) {
			
			String mTime = ((Vector) med.get(i)).get(4).toString();
			String[] clock = mTime.split("\\:");
			String name = ((Vector) med.get(i)).get(1).toString();
			String dose = ((Vector) med.get(i)).get(2).toString();
			int curHour = curTime.get(Calendar.HOUR_OF_DAY);
			int curMinute = curTime.get(Calendar.MINUTE);
			int hour = Integer.parseInt(clock[0]);
			int minute = Integer.parseInt(clock[1]);
			Log.d("clock", "current time: "+curHour+":"+curMinute);
			Log.d("clock", "db time: "+hour+":"+minute);
			if((curHour < hour) || (curHour == hour && curMinute < minute)){
				b = true;
				Log.d("clock", "are you true? .. "+b);
			}
			if(b == true) {
				timeOff.set(Calendar.HOUR_OF_DAY, hour);
				timeOff.set(Calendar.MINUTE, minute);
			
				Log.d("clock", clock[0]);
				Log.d("clock", clock[1]);
				
				Intent intent = new Intent(MedicineReminder.this, medicineReciever.class);
				intent.putExtra("name", name);
				intent.putExtra("dose", dose);
				int id = Integer.parseInt(((Vector) med.get(i)).get(0).toString());
				intent.setData(Uri.parse("timer:" + id));
				
				PendingIntent sender = PendingIntent.getBroadcast(
								                    MedicineReminder.this, 0, intent,
								                    Intent.FLAG_GRANT_READ_URI_PERMISSION);
				
				AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
				am.set(AlarmManager.RTC_WAKEUP, timeOff.getTimeInMillis(), sender);
				Log.d("clock", "Alarm set");
				b = false;
			}
		}
	}
	
	public void checkDB() {
    	mDbHelper = new MedicineDBAdapter(MedicineReminder.this);
        mDbHelper.open();
        try {
         	curSor = mDbHelper.fetchAllMedicines();
         } catch (SQLiteException e) {
         	Log.d("sql", ""+e);
         }
         if(curSor != null) {
        	 	getMedicines();
         }
    }

}
