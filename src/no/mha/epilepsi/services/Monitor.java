/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package no.mha.epilepsi.services;


import android.R.string;
import android.app.Notification;
import android.app.PendingIntent;
import android.os.IBinder;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import no.mha.epilepsi.Activities.EpilepsiPlanlegger;
import no.mha.epilepsi.Activities.MonitorReminder;
import no.mha.epilepsi.Activities.WarnContact;
import no.mha.epilepsi.R;
import no.mha.epilepsi.general.MonitoredSeizure;
import no.mha.epilepsi.handlers.SeizureHandler;



/**
 *
 * @author hallvardwestman
 */
public class Monitor extends Service {
     
    
    public static boolean RUNWHAT = false;
    public static boolean RUN_ALERT_TIMER = false;
    public static boolean CONTINUE_PRESSED = false; //if user press continue on reminder, the alerttimer should stop
    public static int MONITOR_INTERVAL;
    public static int ALERT_LIMIT = 5;
    public static boolean REMINDER_STARTED = false;
    public static int REMINDER_CONTEXT = 1;
    public static int ALERT_CONTEXT = 2;
    public static String CONTACT_PERSON = "";
    public static String CONTACT_NUMBER = "";
    public static int REMINDERINTERVAL_MINUTE = 0;
    public static int REMINDERINTERVAL_SECOND = 10;
    public static int RESPONDINTERVAL_MINUTE = 0;
    public static int RESPONDINTERVAL_SECOND = 10;
    
    
    final static int myID = 1234;
    
    public static SeizureHandler seizureHandler;
    static MonitoredSeizure ms;
    static Monitor monitor; 
    
    
    Handler messageHandler; 
    Context ctx;
    public static Intent reminderActivity, warnContactActivity;
   
                    
    
    
    @Override
    public void onCreate() {
        super.onCreate();
        
        //sets for singleton
        monitor = this;
        
        EpilepsiPlanlegger.MONITOR_RUNNING = true;
        RUN_ALERT_TIMER = true;
        
        
        /*
         * for the new activities to be launched from here
         */
        
        reminderActivity = new Intent(Monitor.this, MonitorReminder.class);
        reminderActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        
        warnContactActivity = new Intent(Monitor.this, WarnContact.class);
        warnContactActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        
        /*
         * setting foreground notification
         */
        
        Notification notification = new Notification(R.drawable.monitor,"Epilepsi",
        System.currentTimeMillis());
        Intent notificationIntent = new Intent(this, Monitor.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        notification.setLatestEventInfo(this, "notificationtitle",
        "notificationmessage", pendingIntent);
        startForeground(myID, notification);
        
        
        /*
         * starts datarecording
         */
        seizureHandler = new SeizureHandler((SensorManager)getSystemService(SENSOR_SERVICE),
                (LocationManager)getSystemService(Context.LOCATION_SERVICE),getApplicationContext());

        
        seizureHandler.startMonitoring();
        
        /*
         * starts the reminder
         */
        reminderTimer();
        
         
     
    }
    /*
     * singleton
     */
    public static Monitor getInstanceOf(){
        return monitor;
    }
    
    /*
     * should stop all recording
     */
    public static void stopMonitor(){
        
        Log.d("Monitor", "stopMonitor_has been called");
        
        EpilepsiPlanlegger.MONITOR_RUNNING = false;
        RUN_ALERT_TIMER = false;
        seizureHandler.stopReading();
        Log.d("StopReading :", " Now storing data");
        getInstanceOf().stopSelf();
        EpilepsiPlanlegger.setIcon();
        
        
        
    }
    
    /*
     * Takes seconds as parameter
     * 
     * 
     */
    
     public static void reminderTimer(){
        
          
         Thread t = new Thread(new Runnable() {
             
             
             int i = 0;
             int interval = (Monitor.REMINDERINTERVAL_MINUTE*60)+Monitor.REMINDERINTERVAL_SECOND;
             
                public void run() {
                     while(i<interval && EpilepsiPlanlegger.MONITOR_RUNNING ){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {

                    }
                     
                     i++;
                     }
                    reminderHandler.sendEmptyMessage(1);
                }
            });
            t.start();
    }

     static Handler reminderHandler = new Handler(){
       
        @Override
        public void handleMessage(Message msg) {
            //NBNBNB
            /*
             * if its beeing called from the standard reminder then it should
             * create new window
             * if its called from the alertTimer, its allready a dialog up
             * 
             */
            if(msg.what == 1 && EpilepsiPlanlegger.MONITOR_RUNNING){
                Monitor.getInstanceOf().startActivity(reminderActivity);
                CONTINUE_PRESSED = false;
                alertTimer();
            }
        }
        
    };
   
    /*
     * checks for user response
     */
    public static void alertTimer(){
        
         Thread t = new Thread(new Runnable() {
            
            int timeLimit = 0;
            int interval = Monitor.RESPONDINTERVAL_MINUTE*60 + Monitor.RESPONDINTERVAL_SECOND;
            
            
            public void run() {
                    
                    while(EpilepsiPlanlegger.MONITOR_RUNNING == true && timeLimit< interval && CONTINUE_PRESSED == false){
                            Log.d("ALERTTIMER :", "starting alertTimer");
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {

                        }
                        timeLimit++;
                        Log.d("ALERTTIMER AFTER SLEEP: = "," "+ EpilepsiPlanlegger.MONITOR_RUNNING);

                        }

                        //if(timeLimit == ALERT_LIMIT && EpilepsiPlanlegger.MONITOR_RUNNING == true){
                            alertHandler.sendEmptyMessage(1);
                        //}
            }
         });
         t.start();
    }
    /*
     * handles the alertTimer thread
     */
    
    static Handler alertHandler = new Handler(){
       
        @Override
        public void handleMessage(Message msg) {
            /*
             * should store monitor as if wer done
             */
            
            if(EpilepsiPlanlegger.MONITOR_RUNNING == true && CONTINUE_PRESSED == false){
                Monitor.getInstanceOf().startActivity(warnContactActivity);
                MonitorReminder.finishIt();
            
            }
        }
    };

    
    
   
    
    


    public void onInit(int i) {}


    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}    
 

