/*
 * one instance contains one monitored session
 */
package no.mha.epilepsi.general;

import java.util.Vector;


/**
 *
 * @author hallvardwestman
 */
public class MonitoredSeizure {
    
    Vector<SensorReading> timeLine;
    
    
    
    public MonitoredSeizure(){
    timeLine = new Vector<SensorReading>();
        
    }
    
    public void putReading(SensorReading reading){
        timeLine.add(reading);
    }

    public Vector<SensorReading> getTimeLine() {
        return timeLine;
    }
    public SensorReading getLastReading(){
        return timeLine.lastElement();
    }
            
    
}
