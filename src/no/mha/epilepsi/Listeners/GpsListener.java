/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package no.mha.epilepsi.Listeners;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;
import java.util.List;

/**
 *
 * @author hallvardwestman
 */
public class GpsListener implements LocationListener{
    
    private Context ctx;
    private Uri curLocation;
    LocationManager locationManager;
    
    public GpsListener(Context c,LocationManager lm){
        ctx = c;
        locationManager = lm;
        setLocation(null);
        
        
    }

    @Override
    public void onLocationChanged(Location loc){

       setLocation(loc);
    }


    @Override
    public void onProviderDisabled(String provider){

        Toast.makeText(ctx,"Gps Disabled",Toast.LENGTH_SHORT ).show();

    }


    @Override
    public void onProviderEnabled(String provider){

    Toast.makeText(ctx,"Gps Enabled",Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras){


    }
    
    
    public void setLocation(Location loc) {
         
        List<String> providers = locationManager.getProviders(true);
        Location l = loc;

        /* Loop over the array backwards, and if you get an accurate location, then break out the loop*/
        
        if (l == null){
            
            for (int i=providers.size()-1; i>=0; i--) {
                    l = locationManager.getLastKnownLocation(providers.get(i));
                    if (l != null) break;
            }
        }
        
        
        if(l != null)
            curLocation = Uri.parse("http://maps.google.com.my/maps?f=d&source=s_d&saddr="+l.getLatitude()+","+l.getLongitude()); 
        else
            curLocation = null;
        
    }
    
    public Uri getLocation(){
        return curLocation;
    }
    public void stop(){
        locationManager.removeUpdates(this);
    }

    public void startGPSlistener(){
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,this);
    }
}
