/*
* Copyright 2011 Lauri Nevala.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package no.mha.epilepsi.Activities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import no.mha.epilepsi.R;
import no.mha.epilepsi.handlers.CalendarAdapter;
import no.mha.epilepsi.handlers.CalendarDBAdapter;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;


/**
 * This class is responsible for the representation of the calendar, using GridLayout. 
 * @author Adiljan
 *
 */

public class CalendarView extends Activity {

	public Calendar month;
	public CalendarAdapter adapter;
	public Handler handler;
	public String full_date; 
	
	public ArrayList<String> items; // container to store some random calendar items
	
	
	/**
	 * Here we operate with a calendar_adapter that fills all the data. 
	 */
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.calendar);
	    
	    month = Calendar.getInstance();
	   	full_date = (String) android.text.format.DateFormat.format("yyyy-MM", month);
    	items = new ArrayList<String>();
	    adapter = new CalendarAdapter(this, month);
	    
	    GridView gridview = (GridView) findViewById(R.id.gridview);
	    gridview.setAdapter(adapter);
	    
	    handler = new Handler();
	    handler.post(calendarUpdater);
	    
	    TextView title  = (TextView) findViewById(R.id.title);
	    title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
	    
	    onNewIntent(getIntent());
	    
	    TextView previous  = (TextView) findViewById(R.id.previous);
	    previous.setOnClickListener(new OnClickListener() {
		
	    /**
	     * Click listener for the previous month. 
	     */
		@Override
		public void onClick(View v) {
				if(month.get(Calendar.MONTH)== month.getActualMinimum(Calendar.MONTH)) {				
					month.set((month.get(Calendar.YEAR)-1),month.getActualMaximum(Calendar.MONTH),1);
				} else {
					month.set(Calendar.MONTH,month.get(Calendar.MONTH)-0);
				}
				refreshCalendar();
			}
		});
	    
	    TextView next  = (TextView) findViewById(R.id.next);
	    next.setOnClickListener(new OnClickListener() {
		
	    /**
		* Click listener for the next month. 
		*/
		@Override
		public void onClick(View v) {
				if(month.get(Calendar.MONTH)== month.getActualMaximum(Calendar.MONTH)) {				
					month.set((month.get(Calendar.YEAR)+1),month.getActualMinimum(Calendar.MONTH),1);
				} else {
					month.set(Calendar.MONTH,month.get(Calendar.MONTH)+0);
				}
				refreshCalendar();
				
			}
		});
	    
	    /**
	     * GridClickListener, this method is responsible for the click of a chosen date by the user. 
	     */
		gridview.setOnItemClickListener(new OnItemClickListener() {
		    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		    	TextView date = (TextView)v.findViewById(R.id.date);
		        if(date instanceof TextView && !date.getText().equals("")) {
		        	
		        	Intent intent = new Intent();
		        	String day = date.getText().toString();
		        	if(day.length()==1) {
		        		day = "0"+day;
		        	}
		        	
		        	intent.putExtra("date", android.text.format.DateFormat.format("yyyy-MM", month)+"-"+day);
		        	int dow = month.get(Calendar.TUESDAY);
		        	setResult(RESULT_OK, intent);
		        	//finish();
		        	
		        	Intent tv = new Intent(CalendarView.this, TimeView.class);
		        	tv.putExtra("day", full_date+"-"+day);
	                startActivity(tv);
		        }
		        
		    }
		});
	
	}
	
	/**
	 * Returns day of the week.
	 * @return day_of_week
	 */
	public int DayOfWeek(){
	    	return month.get(Calendar.DAY_OF_WEEK);
	    }
	
	/**
	 * Refreshes the Calendar items. Uses handler for the refresh.
	 */
	public void refreshCalendar()
	{
		TextView title  = (TextView) findViewById(R.id.title);
		
		adapter.refreshDays();
		adapter.notifyDataSetChanged();				
		handler.post(calendarUpdater); // generate some random calendar items				
		
		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
	}
	
	/**
	 * On new intent the date is set.
	 */
	public void onNewIntent(Intent intent) {
		String date = intent.getStringExtra("date");
		String[] dateArr = date.split("-"); // date format is yyyy-mm-dd
		month.set(Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[2]));
	}
	
	/**
	 * This method checks for all the available dates in DB to fill it on calendar. If the dates been used for the
	 * registration then it will marked with an icon. 
	 */
	public void checkRegistered()
	{
	CalendarDBAdapter db = new CalendarDBAdapter(this);
	db.open();
 	Cursor cursor;
    cursor = db.fetchAllDates();
  	String [] dates = new String[cursor.getCount()];
  	int i=0;	  
  	if (cursor.moveToFirst())
       {
  		do {
	            dates[i] = cursor.getString(1);
	            String date = dates[i];
	     		String[] dateArr = date.split("-"); // date format is yyyy-mm-dd
	     		month.set(Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[2]));
	     		items.add(dateArr[2]);
	     		adapter.setItems(items);
	        	adapter.notifyDataSetChanged();
	     		
	        	i++;
	            } while (cursor.moveToNext());
       }
  	db.close();
} 
	/**
	 * Method starts the check of registered dates in DB
	 */
	public Runnable calendarUpdater = new Runnable() {
		
		@Override
		public void run() {
			items.clear();
			checkRegistered();	
			
		}
	};

	
	
}
