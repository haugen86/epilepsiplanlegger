package no.mha.epilepsi.Activities;

import no.mha.epilepsi.R;
import no.mha.epilepsi.handlers.MedicineDBAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

public class Medicine extends Activity {
	
	Button addMedicine;
	EditText editName;
	EditText editDose;
	
	String time;
	String sDays = "";
	String dose;
	String name;
		
	AlertDialog.Builder builder;
    AlertDialog alertDialog;
	
	final CharSequence[] days = {"Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag", "Søndag"};
    
    final int monday 	= 0;
    final int tuesday 	= 1;
    final int wednesday = 2;
    final int thursday 	= 3;
    final int friday 	= 4;
    final int saturday 	= 5;
    final int sunday 	= 6;
    int i = 0;
    
    final Boolean[] state 	= new Boolean[7];
    
    private MedicineDBAdapter mDbHelper;
    Cursor curSor;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medicine);
        
       showMedicine();
       
       Log.d("dag", "onClick: starting srvice");
       
       
        
        Context mContext = this;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.add_medicine,(ViewGroup) findViewById(R.id.add_medicine_root));
        editName = (EditText) layout.findViewById(R.id.medicineName); 
        editDose = (EditText) layout.findViewById(R.id.dose); 
        
        
        state[monday] 			= false;
        state[tuesday]	 		= false;
        state[wednesday] 		= false;
        state[thursday] 		= false;
        state[friday] 			= false;
        state[saturday] 		= false;
        state[sunday] 			= false;
        
        
        final TimePicker mTimePicker = (TimePicker) layout.findViewById(R.id.timepicker);
        mTimePicker.setIs24HourView(true);
        builder = new AlertDialog.Builder(mContext); 
        builder.setView(layout);
        builder.setMultiChoiceItems(days, new boolean[] { false, false, false,
                false, false, false, false }, new DialogInterface.OnMultiChoiceClickListener() {
        	
        	@Override
            public void onClick(DialogInterface dialog, int which,
                    boolean isChecked) {
                state[which] = isChecked;
            }
        });
        
        builder.setPositiveButton("Lagre",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                    	
                    	time = ""+mTimePicker.getCurrentHour()+":"+mTimePicker.getCurrentMinute();
                    	String[] timeArr = time.split("\\:");
                    	Log.d("Time", time);
                    	if(timeArr[1].length() < 2) {
                    		time = timeArr[0]+":0"+timeArr[1];
                    	}
                    
                    	
                    	
                    	name = editName.getText().toString();
                    	dose = editDose.getText().toString();
                    	Log.d("Name", name);
                    	
                    	// prepare yourself for the ugliest and longest if-statement!!! copyright: Marius Haugen Engen
                    	
                    	if(state[monday]) {
                    			sDays = "Ma";
                    	} if(state[tuesday]) {
                    		if(sDays == "") {
                    			sDays = "Ti";
                    		}else {
                    			sDays += ", Ti";
                    		}
                    	} if(state[wednesday]) {
                    		if(sDays == "") {
                    			sDays = "On";
                    		}else {
                    			sDays += ", On";
                    		}                    		
                    	} if(state[thursday]) {
                    		if(sDays == "") {
                    			sDays = "To";
                    		}else {
                    			sDays += ", To";
                    		}                    		
                    	} if(state[friday]) {
                    		if(sDays == "") {
                    			sDays = "Fr";
                    		}else {
                    			sDays += ", Fr";
                    		} 
                    	} if(state[saturday]) {
                    		if(sDays == "") {
                    			sDays = "Lø";
                    		}else {
                    			sDays += ", Lø";
                    		}                    		
                    	} if(state[sunday]) {
                    		if(sDays == "") {
                    			sDays = "Sø";
                    		}else {
                    			sDays += ", Sø";
                    		}
                    	}	
                    	Log.d("Days", sDays);
                    	
                    	mDbHelper = new MedicineDBAdapter(Medicine.this);
                        mDbHelper.open();
                        mDbHelper.createMedicine(name, dose, sDays, time);
                        mDbHelper.close();
                        
                    	
                        /*databaseKlasse.LagreDetHer(
                        		 	state[monday],
                        	        state[thursday],
                        	        state[wednesday],
                        	        state[thursday],
                        	        state[friday],
                        	        state[saturday],
                        	        state[sunday]);*/
                        
                        addLast();
                        
                        sDays = "";
                        time = "";
                        name = "";
                        dose = "";
                    }
                });
        final AlertDialog alert = builder.create();
        
        addMedicine = (Button)findViewById(R.id.addMedicine);
        addMedicine.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	alert.show();
            }
        });
    }
    
    public void showMedicine() {
    	
    	 mDbHelper = new MedicineDBAdapter(Medicine.this);
         mDbHelper.open();
         try {
         	curSor = mDbHelper.fetchAllMedicines();
         } catch (SQLiteException e) {
         	Log.d("sql", ""+e);
         }
         	if(curSor != null) {
         		Log.d("sql", "lol");
         		
 	        	curSor.moveToFirst();
 		        TableLayout view = (TableLayout) findViewById(R.id.dataTable);
 		       
 		        while (curSor.isAfterLast() == false) {   	
 		        	TableRow tr = new TableRow(this);
 		            view.addView(tr);
 		           final long id = Long.valueOf(curSor.getString(0));
 		           
 		           /*
 		            * WOOPS, WE FORGOT TO ADD DELETION UNTIL 1 HOUR BEFORE THE DEADLINE
 		            * BELOW IS A CHEAP HACK JUST TO ADD THE FUNCTIONALIY!!!
 		            */
 		           tr.setOnClickListener(new  OnClickListener() {

 			            @Override
 			            public void onClick(View v) {
 			            	
 			            	
 			            	  mDbHelper.open();
 			                mDbHelper.deleteMedicine(id);

 			            }
 			        });
 		            while(curSor.getColumnCount() > i) {
 		            	int row = i+1;
 		            	TextView td = new TextView(this);
 		            	tr.addView(td);
 		            	td.setText(curSor.getString(i));
 		            	td.setLayoutParams(new TableRow.LayoutParams(row));
 		            	i++;
 		            }
 		            i = 0;
 		       	    curSor.moveToNext();
 		        }
         	}
         	mDbHelper.close();
    }
    
    public void addLast() {
    	mDbHelper = new MedicineDBAdapter(Medicine.this);
        mDbHelper.open();
        try {
         	curSor = mDbHelper.fetchAllMedicines();
         } catch (SQLiteException e) {
         	Log.d("sql", ""+e);
         }
         	if(curSor != null) {
         		Log.d("sql", "lol");
 	        	curSor.moveToLast();
 		        TableLayout view = (TableLayout) findViewById(R.id.dataTable);
		    	TableRow r = new TableRow(this);
		        view.addView(r);
		        
		        r.setPadding(0, 0, 5, 0);
		        
		        TextView text1 = new TextView(this);
		        TextView text2 = new TextView(this);
		        TextView text3 = new TextView(this);
		        TextView text4 = new TextView(this);
		        TextView text5 = new TextView(this);
		        
		        r.addView(text1);
		        r.addView(text2);
		        r.addView(text3);
		        r.addView(text4);
		        r.addView(text5);
		        
		        r.setOnClickListener(new  OnClickListener() {

		            @Override
		            public void onClick(View v) {
		            	long id = curSor.getLong(0);
		            	mDbHelper.deleteMedicine(id);
		                Log.d("delete", "sletting av: "+id);

		            }
		        });
		        
		        text1.setText(curSor.getString(0));
		        text1.setPadding(3, 3, 3, 3);
		        text1.setLayoutParams(new TableRow.LayoutParams(1));
		        
		        text2.setText(curSor.getString(1));
		        text2.setPadding(3, 3, 3, 3);
		        text2.setLayoutParams(new TableRow.LayoutParams(2));
		        
		        text3.setText(curSor.getString(2));
		        text3.setPadding(3, 3, 3, 3);
		        text3.setLayoutParams(new TableRow.LayoutParams(3));
		        
		        text4.setText(curSor.getString(3));
		        text4.setPadding(3, 3, 3, 3);
		        text4.setLayoutParams(new TableRow.LayoutParams(4));
		        
		        text5.setText(curSor.getString(4));
		        text5.setPadding(3, 3, 3, 3);
		        text5.setLayoutParams(new TableRow.LayoutParams(5));
        }
        mDbHelper.close();    	
    }
}
