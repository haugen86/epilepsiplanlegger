package no.mha.epilepsi.Activities;

import no.mha.epilepsi.services.MedicineReminder;
import no.mha.epilepsi.services.Monitor;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import no.mha.epilepsi.R;
import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import java.util.Locale;


/**
 * 
 * @author Adiljan, Hallward and Marius
 * The main activity that includes main function buttons, tips text and welcome image.
 * All buttons include action listener for the further navigation to methods.
 *
 */

/*
 * Main activity uses main.xml layout
 */
public class EpilepsiPlanlegger extends Activity implements TextToSpeech.OnInitListener{
    /** Called when the activity is first created. */
	static final int DATE_DIALOG_ID = 0;
	static final int PICK_DATE_REQUEST = 1;
        static EpilepsiPlanlegger epilepsiPlanlegger;
        
        public UserSettings usrSet;

    static EpilepsiPlanlegger getInstanceOf() {
        return epilepsiPlanlegger;
    }
	protected int mYear, mMonth, mDay;
        public static boolean MONITOR_RUNNING = false;
        
        private static final int MY_DATA_CHECK_CODE = 1234;
        private static TextToSpeech mTts;
        
        
        
        private static Button monitor;
        
    /** Below are all four buttons that will be used on main page
    * they are:
    * diary, monitor, medicines and new
    * 
    * Diary - calendar for the overview of the logs done before and certainly it will possibility
    * to add new. Button will navigate to calendar activity.*/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //TextView text = new TextView(this);
        //text.setText("Hello, Epileptikk");
        //setContentView(text);
         

        final Calendar c = Calendar.getInstance();
    	mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        epilepsiPlanlegger = this;
       startService(new Intent(this, MedicineReminder.class));

        /* Below are all four buttons that will be used on main page
         * they are:
         * diary, monitor, medicines and new
         * 
         * Diary - calendar for the overview of the logs done before and certainly it will possibility
         * to add new. Button will navigate to calendar activity.*/

               
        Button diary = (Button)findViewById(R.id.diary);
        diary.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	Intent intent = new Intent(v.getContext(),CalendarView.class);
				
	    		intent.putExtra("date", mYear+"-"+mMonth+"-"+mDay);
	    		startActivityForResult(intent, PICK_DATE_REQUEST);	      
            
            }
        });
        /* Monitor will activate all needed sensors, here accelerometer and GPS. 
         * Activity will be run in background.
         */
        
	        monitor = (Button)findViewById(R.id.monitor);
	        monitor.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                                
                if(!MONITOR_RUNNING){
                    MONITOR_RUNNING = true;
                    Intent mon = new Intent(EpilepsiPlanlegger.this, Monitor.class);
                    startService(mon);
                }
                else{
                    MONITOR_RUNNING = false;
                    Monitor.stopMonitor();
                }
                setIcon();
                    
            }
            
        });
        
                
        
        
        /* List medicines will be displayed with the order of nearest medicine consumption. 
         * It will display image, name, dose, day and time of a medicine. 
         */
        Button medicines = (Button)findViewById(R.id.medicines);
        medicines.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	Intent med = new Intent(EpilepsiPlanlegger.this, Medicine.class);
                startActivity(med);	    
            }
        });
        
        /* This button is used for adding activities or conditions done by the user recently.
         * It was placed here for quick logging of recent changes in his life.
         */
        Button add = (Button)findViewById(R.id.add);
        add.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	Intent add = new Intent(EpilepsiPlanlegger.this, ChooseView.class);
            	add.putExtra("day", android.text.format.DateFormat.format("yyyy-MM-dd", c));
            	           	
            	SimpleDateFormat df = new SimpleDateFormat("HH");
                String formattedDate = df.format(c.getTime());
                // formattedDate have current date/time
                add.putExtra("time", formattedDate+":00");
                            	
            	Toast.makeText(getApplicationContext(), "this is con-"+formattedDate, Toast.LENGTH_SHORT).show();
                startActivity(add);
            }
        });
        
        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
         
    }
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == PICK_DATE_REQUEST) {
            if (resultCode == RESULT_OK) {
            	String[] dateArr = data.getStringExtra("date").split("-");
            	Intent tv = new Intent(EpilepsiPlanlegger.this, TimeView.class);
                startActivity(tv);
            	
            
            	//DatePicker dp = (DatePicker)findViewById(R.id.datePicker1);
            	//dp.updateDate(Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[2])); 
            
            }
		}
                if (requestCode == MY_DATA_CHECK_CODE)
        {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS)
            {
                // TTS installed, create instance
                mTts = new TextToSpeech(this, this);
                mTts.setLanguage(Locale.ENGLISH);
                int duration = Toast.LENGTH_SHORT;
            
            }
            else
            {
                // TTS not installed, install it

                Context context = getApplicationContext();
                CharSequence text = "TTS is NOT installed";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                Intent installIntent = new Intent();
                installIntent.setAction(
                        TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
	}
    @Override
    protected void onDestroy() {


    //Close the Text to Speech Library
    if(mTts != null) {

        mTts.stop();
        mTts.shutdown();
        Log.d("destroying monitorreminder", "TTS Destroyed");
    }
    super.onDestroy();
}
    
    public static void setIcon(){
                
                if(MONITOR_RUNNING){
                    
                    monitor.setBackgroundResource(R.drawable.monitor);
                }
                else{
                    
                    monitor.setBackgroundResource(R.drawable.monitor_disable);
                }
            }
      @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void onInit(int arg0) {
    }
    public static void alarmUser(String st){
        
        mTts.speak(st,TextToSpeech.QUEUE_FLUSH,null);
    }

    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.epilepsiplanlegger_optionmenu, menu);
    return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
    case R.id.user_settings:
        
        Intent user = new Intent(EpilepsiPlanlegger.this, UserSettings.class);
            startActivity(user);
                    
        return true;
   
    default:
        return super.onOptionsItemSelected(item);
    }
}

}