/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package no.mha.epilepsi.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import java.util.ArrayList;
import java.util.List;
import no.mha.epilepsi.R;
import no.mha.epilepsi.handlers.UserSettingsAdapter;
import no.mha.epilepsi.services.Monitor;

/**
 *
 * @author hallvardwestman
 */
public class UserSettings extends Activity {
    
    private UserSettingsAdapter mDbHelper;
    Cursor curSor;
    
    String name;
    TimePicker reminderInterval;
    TimePicker respondInterval;
    Button choose,submit;
    TextView contactname;
    private static final int CONTACT_PICKER_RESULT = 1001; 
    Intent contactPickerIntent;
    
    public static String contactNumber;
    public static String contactName;

   public static UserSettings userSettings;
    
    
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.usersettings);
        userSettings = this;
        
       
        
        reminderInterval = (TimePicker)findViewById(R.id.reminderinterval);
        reminderInterval.setIs24HourView(true);
        
        
        respondInterval = (TimePicker)findViewById(R.id.respondinterval);
        respondInterval.setIs24HourView(true);
        
        
        
        
        contactname =(TextView)findViewById(R.id.contactpersonview);
        
        getSettingsFromDb();
        
        
        
        choose = (Button) findViewById(R.id.choosebutton);
        choose.setOnClickListener(new OKListener());
        
        submit = (Button) findViewById(R.id.submitbutton);
        submit.setOnClickListener(new OKListener());
        
       
        
      
        
    }
    @Override
    public void onResume(){
        super.onResume();
        //getSettingsFromDb(); 
        
        
    }

    public void getSettingsFromDb() throws SQLException, NumberFormatException {
        
        mDbHelper = new UserSettingsAdapter(this);
        mDbHelper.open();
        
        curSor = mDbHelper.fetchSettings();

        
        if(curSor != null) {
            
            Log.d("sql", "lol");
            curSor.moveToFirst();
           int i = 0;
            while (curSor.isAfterLast() == false) {   
                while(curSor.getColumnCount() > i) {
                    int row = i+1;
                    
                    switch(i){
                        case 1:
                            Monitor.CONTACT_PERSON = curSor.getString(i);
                            Log.d("setting contact", Monitor.CONTACT_PERSON);
                        break;
                        case 2:
                            Monitor.CONTACT_NUMBER = curSor.getString(i);
                            Log.d("setting contact", Monitor.CONTACT_NUMBER);
                        break;
                        case 3:
                            Monitor.REMINDERINTERVAL_MINUTE = curSor.getInt(i);
                            Log.d("setting contact", Monitor.REMINDERINTERVAL_MINUTE+"");
                        break;
                        case 4:
                            Monitor.REMINDERINTERVAL_SECOND = curSor.getInt(i);
                            Log.d("setting contact", Monitor.REMINDERINTERVAL_SECOND+"");
                        break;
                        case 5:
                            Monitor.RESPONDINTERVAL_MINUTE = curSor.getInt(i);
                            Log.d("setting contact", Monitor.RESPONDINTERVAL_MINUTE+"");
                        break;
                        case 6:
                            Monitor.REMINDERINTERVAL_SECOND = curSor.getInt(i);
                            Log.d("setting contact", Monitor.RESPONDINTERVAL_SECOND+"");
                        break;
                    }
                    
                    i++;
                }
                i = 0;
                curSor.moveToNext();
            }
            
        }else{
            
        }
        
        
        
        
    }
    private class OKListener implements android.view.View.OnClickListener {
        @Override
        public void onClick(View v) {
            
            switch(v.getId()){
                
                case R.id.choosebutton:{
                    
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.ContactsContract.Contacts.CONTENT_URI); 
                    
                    startActivityForResult(intent, CONTACT_PICKER_RESULT);
                    
                    break;
                }
                case R.id.submitbutton:{
                   
                    try {
                        
                        Monitor.CONTACT_PERSON = contactName;
                        Monitor.CONTACT_NUMBER = contactNumber;
                            Monitor.REMINDERINTERVAL_MINUTE = reminderInterval.getCurrentHour();
                            Monitor.REMINDERINTERVAL_SECOND = reminderInterval.getCurrentHour();
                            Monitor.RESPONDINTERVAL_MINUTE = respondInterval.getCurrentHour();
                            Monitor.RESPONDINTERVAL_SECOND = respondInterval.getCurrentMinute();
                        
                     mDbHelper.createSettings(Monitor.CONTACT_PERSON,Monitor.CONTACT_NUMBER,
                            Monitor.REMINDERINTERVAL_MINUTE,
                            Monitor.REMINDERINTERVAL_SECOND,
                            Monitor.RESPONDINTERVAL_MINUTE,
                            Monitor.RESPONDINTERVAL_SECOND);
                    
                    mDbHelper.close();

                   } catch (SQLiteException e) {
                       Log.d("sql", ""+e);
                    }
                    UserSettings.getInstanceOf().finish();
                    break;
                
                
                }
                
                    
            }
            
        }
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        
       String nameit = new String();
    if (resultCode == RESULT_OK) {  
        switch (requestCode) {  
        case CONTACT_PICKER_RESULT:
            //final TextView phoneInput = (TextView) findViewById(R.id.contactpersonview);
            Cursor cursor = null;  
            String phoneNumber = "";
            List<String> allNumbers = new ArrayList<String>();
            int phoneIdx = 0;
            try {  
                
                Uri result = data.getData();  
                String id = result.getLastPathSegment();  
                cursor = getContentResolver().query(Phone.CONTENT_URI, null, Phone.CONTACT_ID + "=?", new String[] { id }, null); 
               
                
                phoneIdx = cursor.getColumnIndex(Phone.DATA);
                if (cursor.moveToFirst()) {
                    contactName = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
                    while (cursor.isAfterLast() == false) {
                        phoneNumber = cursor.getString(phoneIdx);
                        contactNumber = phoneNumber;
                        allNumbers.add(phoneNumber);
                        cursor.moveToNext();
                    }
                } else {
                    //no results actions
                }  
            } catch (Exception e) {  
               //error actions
            } finally {  
                if (cursor != null) {  
                    cursor.close();
                }

                final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
                AlertDialog.Builder builder = new AlertDialog.Builder(UserSettings.this);
                builder.setTitle("Choose a number");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        String selectedNumber = items[item].toString();
                        contactNumber = selectedNumber;
                        selectedNumber = selectedNumber.replace("-", "");
                        //phoneInput.setText(selectedNumber);
                        
                    }
                });
                AlertDialog alert = builder.create();
                if(allNumbers.size() > 1) {
                    alert.show();
                } else {
                    String selectedNumber = phoneNumber.toString();
                    contactNumber = selectedNumber;
                    selectedNumber = selectedNumber.replace("-", "");
                    //phoneInput.setText(nameit+" "+selectedNumber);
                    contactNumber = selectedNumber;
                }

                if (phoneNumber.length() == 0) {  
                    //phoneInput.setText(" Haz no number");
                    contactNumber = "Haz no number";
                    contactName = "";
                }  
                
                contactname.setText(contactName);
            }  
            break;  
        }  
    } else {
       //activity result error actions
    }
    
    
    }
public static UserSettings getInstanceOf(){
    return userSettings;
}


}
