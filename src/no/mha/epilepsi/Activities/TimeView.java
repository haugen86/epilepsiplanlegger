package no.mha.epilepsi.Activities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import no.mha.epilepsi.R;
import no.mha.epilepsi.handlers.CalendarDBAdapter;
import no.mha.epilepsi.handlers.MedicineDBAdapter;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * This class is responsible for the representation of 24 hours in list layout.
 * The list lists all data registered for the specific time.
 * @author Adiljan
 *
 */
public class TimeView extends ListActivity{
		
	String day;
	String[] TME_LIST;
	
	/**
	 * Here all the needed data are registered. TME_LIST array is defined for the representation of a day as a 24 hours time.
	 * Gets all parsed data from other intents.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	
	TME_LIST = new String[] {
			  "00:00", "01:00", "02:00", "03:00", "04:00",
			  "05:00", "06:00", "07:00", "08:00", "09:00",
			  "10:00", "11:00", "12:00", "13:00", "14:00",
			  "15:00", "16:00", "17:00", "18:00", "19:00",
			  "20:00", "21:00", "22:00", "23:00",
		  };
	
	
	Bundle extras = getIntent().getExtras();
	if(getIntent().getExtras()==null){
		day = "19-12-2011";
		finish();
	}
	else	
		day = extras.getString("day");
	checkRegistered(day);

	setListAdapter(new ArrayAdapter<String>(this, R.layout.time_view, TME_LIST));

	ListView lv = getListView();
	lv.setTextFilterEnabled(true);
	
	/**
	 * List listener. It is responsible for listening of click to the specific time chosen.
	 * The chosen time and day will be sent to the next intent (activity).
	 * New activity will be started.
	 */
	lv.setOnItemClickListener(new OnItemClickListener() {
	public void onItemClick(AdapterView<?> parent, View view,
	int position, long id) 
	{
		String test = (String) ((TextView) view).getText();
		Intent cv = new Intent(TimeView.this, ChooseView.class);
		cv.putExtra("time", test);
		cv.putExtra("day", day);
        startActivity(cv);
    }
	});

	}
	
	/**
	 * This method will check all data in the DB and will find the specific object that equals chosen day.
	 * @param day chosen day 
	 */
	public void checkRegistered(String day)
	{
			CalendarDBAdapter db = new CalendarDBAdapter(this);
			db.open();
		 	Cursor cursor;
	        cursor = db.fetchAllDates();
	      	String [] choice = new String[cursor.getCount()];
	      	
	      	int i=0;	  
	      	if (cursor.moveToFirst())
   	        {
   		      do {
   		    	
   		    	 if(cursor.getString(1).equals(day))
   		    	 {
   		    		 choice[i] = cursor.getString(3);
   		    	 }
   	            
   		    	 for (int j=0; j<24; j++){
   		    		if (TME_LIST[j].equals(choice[i])){
   		    			TME_LIST[j]=TME_LIST[j]+" "+cursor.getString(2);
   		    		}
   		    	}
   		    	i++;
   	            } while (cursor.moveToNext());
   	        }
	      	db.close();
	      	
	      	int tod = FormatDate(day);
	      	String [] ymd = day.split("-");
	      	MedicineDBAdapter dbm = new MedicineDBAdapter(this);
			
			 
			dbm.open();
			Cursor c;
			c = dbm.fetchAllMedicines();
			String [] med_data = new String [c.getCount()];
			String [] med_time = new String [c.getCount()];
			String [] med_name = new String [c.getCount()];
			int n=0;
			 
			
			if (c.moveToFirst()){
				do {
					med_name [n] = c.getString(1);
					med_data [n] = c.getString(3);
					med_time [n] = c.getString(4);
					String [] med_t = med_time[n].split(":");
					med_time[n] = med_t[0];
					String [] med_d = med_data[n].split(", ");
					
					if(med_time[n].length()==1) {
						 med_time[n] = "0"+med_time[n];
				    }
					
					for(int j=0; j<med_d.length; j++ ){
						
						int dow = test(med_d[j]);
												
						if (dow==tod)
							{
								String [] check;
								for (int l=0; l<24; l++){
									check = TME_LIST[l].split(":");
									if (check[0].equals(med_time[n])){
				   		    			TME_LIST[l]=TME_LIST[l]+" "+c.getString(1);
				   		    		}
								}
							}
						}
					n++;
				} while (c.moveToNext());
			
			}
			dbm.close();
	      	 
	} 
	
	/**
	 * Here the formatted representation of the day and its return of value according to the dates day of week.   
	 * @param fdate date that will be compared 
	 * @return day of week as an integer where Sunday is 1
	 */
	public int FormatDate(String fdate){
		try {
			
		  DateFormat formatter ; 
		  Date date ; 
		  formatter = new SimpleDateFormat("yyy-MM-dd");
		  date = (Date)formatter.parse(fdate); 
		 Calendar cal=Calendar.getInstance();
		 cal.setTime(date);
		 
		 return cal.get(Calendar.DAY_OF_WEEK);
		  }
		  catch (ParseException e)
		  {System.out.println("Exception :"+e);  }
		return 0;
		 
		 }
		
	/**
	 * Checks for the string representation of day of week that will be returned as an integer.
	 * @param day_of_week the string that have to be represented as integer
	 * @return integer of day of week representation
	 */
	public int test(String day_of_week){
		int d=0;
		
		if(day_of_week.equals("Ma"))
			d = 2;
		else if(day_of_week.equals("Ti"))
			d = 3;
		else if(day_of_week.equals("On"))
			d = 4;
		else if(day_of_week.equals("To"))
			d = 5;
		else if(day_of_week.equals("Fr"))
			d = 6;
		else if(day_of_week.equals("Lø"))
			d = 7;
		else if(day_of_week.equals("Sø"))
			d = 1;
	
		return d;
	}
}
