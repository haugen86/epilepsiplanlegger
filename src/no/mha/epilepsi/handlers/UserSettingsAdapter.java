/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package no.mha.epilepsi.handlers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 *
 * @author hallvardwestman
 */
public class UserSettingsAdapter {
    
    public static final String KEY_CONTACTNAME = "contactname";
    public static final String KEY_CONTACTNUMBER = "contactnumber";
    public static final String KEY_REMINDERINTERVAL_M = "reminderinterval_m";
    public static final String KEY_REMINDERINTERVAL_S = "reminderinterval_s";
    public static final String KEY_RESPONDINTERVAL_M = "respondinterval_m";
    public static final String KEY_RESPONDINTERVAL_S = "respondinterval_s";

     private static final String TAG = "UserSettingsAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    /**
     * Database creation sql statement
     */

    private static final String DATABASE_CREATE = "create table if not exists "
            + "usersettings ( id integer primary key autoincrement, "
            + "contactname text not null, contactnumber text not null, "
            + "reminderinterval_m integer not null, reminderinterval_s integer not null, "
            + "respondinterval_m integer not null, respondinterval_s integer not null);";
    
    
    
    private static final String DATABASE_NAME = "usersettings";
    private static final String DATABASE_TABLE = "usersettings";
    private static final int DATABASE_VERSION = 2;

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            
        }

        @Override
        public void onCreate(SQLiteDatabase db){

             Log.d("clock", "hello");
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS usersettings");
            onCreate(db);
        }
    }
    
    public UserSettingsAdapter(Context ctx) {
        this.mCtx = ctx;
    }
    
    public UserSettingsAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }
    public long createSettings(String cnam,String cnum,int rem_m,int rem_s,int res_m,int res_s) {

        
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_CONTACTNAME, cnam);
        initialValues.put(KEY_CONTACTNUMBER, cnum);
        initialValues.put(KEY_REMINDERINTERVAL_M, rem_m);
        initialValues.put(KEY_REMINDERINTERVAL_S, rem_s);
        initialValues.put(KEY_RESPONDINTERVAL_M, res_m);
        initialValues.put(KEY_RESPONDINTERVAL_S, res_s);
        
        Log.d("DB", ""+initialValues);
        
        long lol = mDb.insert(DATABASE_TABLE, null, initialValues);
        
        Log.d("insert", ""+lol);
        return lol;
    }
    
    public Cursor fetchSettings() throws SQLException {
    	Cursor mCursor;
        mCursor = mDb.query(DATABASE_TABLE, null, null, null, null, null, null);
    	Log.d("sql", ""+mCursor);
    	return mCursor;
    }
    public int deleteSettings() {

        return mDb.delete(DATABASE_TABLE,null,null);
    }
    
    
    
}
