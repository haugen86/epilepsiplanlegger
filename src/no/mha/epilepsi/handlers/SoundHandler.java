/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package no.mha.epilepsi.handlers;

import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 *
 * @author http://code.google.com/p/android-labs/source/browse/trunk/
 *         NoiseAlert/src/com/google/android/noisealert/SoundMeter.java
 * @editor Hallvard Westman
 */

public class SoundHandler extends Handler {
        static final private double EMA_FILTER = 0.6;

        private MediaRecorder mRecorder = null;
        private double mEMA = 0.0;
        
        boolean runit = false;
        Handler messageHandler;
        
        double curdecibel;
        
        
        /*
         * sets the handler
         */
        
        public SoundHandler(){
            
            messageHandler = this;
            
        }
        
        /**
         * Updating current decibel
         *
         */
        
        public void startDecibelListener() {
            
            runit= true;
            
            if (mRecorder == null) 
                prepareRecorder();
            
            Thread t = new Thread(new Runnable() {

                    public void run() {
                        runit = true;

                        while(runit){
                            Message m = new Message();
                            Bundle b = new Bundle();
                            b.putDouble("dB", getAmplitudeEMA());
                            m.setData(b);
                            
                            messageHandler.sendMessage(m);

                        try {
                            Thread.sleep(150);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(SoundHandler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        }
                    }
                });
                t.start();

            
                
        }

    public void prepareRecorder() throws IllegalStateException {
        /*
         * setting up the stream from mic
         */
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile("/dev/null"); 
        
            try {
                mRecorder.prepare();
            } catch (IllegalStateException ex) {
                Logger.getLogger(SoundHandler.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SoundHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        mRecorder.start();
        mEMA = 0.0;
    }
        
        public void stop() {
            runit= false;
                if (mRecorder != null) {
                        mRecorder.stop();       
                        mRecorder.release();
                        mRecorder = null;
                }
        }
        
        /*
         * fetching data from the mic
         */
        
        /*
         * converting result
         */
        public double getAmplitudeEMA() {
                double amp = (mRecorder.getMaxAmplitude());
                
                double dB = 20 * Math.log10(amp/10);
                return dB;
        }
        
        
        /*
         * recieves result from recording thread
         */
        
        @Override
        public void handleMessage(Message msg) {  
            
            curdecibel = msg.getData().getDouble("dB");
        }
        
        public double getDecibel(){
            return curdecibel;
        }
         
        
        
}