/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package no.mha.epilepsi.handlers;

import android.content.Context;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.net.Uri;
import android.util.Log;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.mha.epilepsi.Listeners.AccelerationListener;
import no.mha.epilepsi.Listeners.GpsListener;
import no.mha.epilepsi.general.MonitoredSeizure;
import no.mha.epilepsi.general.SensorReading;

/**
 *
 * @author hallvardwestman
 */
public class SeizureHandler {
    
    MonitoredSeizure seizure;
    AccelerationListener al;
    SoundHandler sh;
    GpsListener gl;
    boolean runit = false;
    Thread t;
    private static int POLL_INTERVAL = 500;
    public static SensorReading lastReading; //beeing requested from WarnContact
  
    double b;
    Uri c;
    
    public SeizureHandler(SensorManager sm,LocationManager lm, Context ctx){
        
        sh = new SoundHandler();
        al = new AccelerationListener(sm);//AccelerationListener(sm);
        gl = new GpsListener(ctx,lm);
        seizure = new MonitoredSeizure();
        
        lastReading = new SensorReading();
        
    }
    
    /*
     * starts the monitoring on a seperate thread
     */
    
    public void startMonitoring(){
        al.startAccelerationListening();
        sh.startDecibelListener();
        gl.startGPSlistener();
       
        /*
         * run aslong as user dont abort
         * 
         */
        runit=true;
            t = new Thread(new Runnable() {

                public void run() {
                    while(runit){
                        
                        /*
                         * sleeps to set pollrate
                         */
                        try {
                            Thread.sleep(POLL_INTERVAL);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(SeizureHandler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        setReading();
                    }
                }
            });
            t.start();
        
    }
    
    /*
     * Collects a single reading an stores it in the total seizure
     * 
     */
    public void setReading(){
        
        lastReading = new SensorReading(al.returnAcceleration(), sh.getDecibel(), gl.getLocation());
        seizure.putReading(lastReading); 
        
    }
    /*
     * call to stop
     */
    public void stopReading(){
        al.stopListening();
        sh.stop();
        gl.stop();
        runit = false;
        
    }
    /* 
     * should sumarize after completing recording
     */
    public MonitoredSeizure moveToDB(){
     
     return seizure;   
        
    }
    public Uri getLocation(){
        //SensorReading s = seizure.getLastReading();
        return lastReading.getLocation();
    }
    
    
}
